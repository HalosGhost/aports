# Contributor: Konstantin Kulikov <k.kulikov2@gmail.com>
# Maintainer: Konstantin Kulikov <k.kulikov2@gmail.com>
pkgname=grafana
pkgver=7.3.3
pkgrel=0
_commit=2489dc4d3a # git rev-parse --short HEAD
_stamp=1605615502 # git --no-pager show -s --format=%ct
pkgdesc="Open source, feature rich metrics dashboard and graph editor"
url="https://grafana.com"
arch="all !mips !mips64" # go is missing
license="Apache-2.0"
makedepends="go"
install="$pkgname.pre-install"
subpackages="$pkgname-openrc"
options="net chmod-clean"
source="$pkgname-$pkgver.tar.gz::https://github.com/grafana/grafana/archive/v$pkgver.tar.gz
	$pkgname-$pkgver-bin.tar.gz::https://dl.grafana.com/oss/release/grafana-$pkgver.linux-amd64.tar.gz
	$pkgname.initd
	$pkgname.confd"

export GOPATH=${GOPATH:-$srcdir/go}
export GOCACHE=${GOCACHE:-$srcdir/go-build}
export GOTMPDIR=${GOTMPDIR:-$srcdir}

# secfixes:
#   7.0.2-r0:
#     - CVE-2020-13379
#   6.3.4-r0:
#     - CVE-2019-15043

build() {
	local ldflags="-X main.version=$pkgver -X main.commit=$_commit -X main.buildstamp=$_stamp"
	go build -ldflags "$ldflags" -v github.com/grafana/grafana/pkg/cmd/grafana-server
	go build -ldflags "$ldflags" -v github.com/grafana/grafana/pkg/cmd/grafana-cli
}

check() {
	local pkgs="./..."
	case "$CARCH" in
	# https://github.com/grafana/grafana/issues/26389
	x86) pkgs="$(go list ./... | grep -Ev '(pkg/tsdb$)')" ;;
	# https://github.com/grafana/grafana/issues/26390
	s390x) pkgs="$(go list ./... | grep -Ev '(pkg/tsdb/influxdb/flux$)')" ;;
	esac

	go test $pkgs
}

package() {
	install -Dm755 "$srcdir/$pkgname.initd" "$pkgdir/etc/init.d/$pkgname"
	install -Dm644 "$srcdir/$pkgname.confd" "$pkgdir/etc/conf.d/$pkgname"
	install -Dm755 "$builddir/$pkgname-server" "$pkgdir/usr/sbin/$pkgname-server"
	install -Dm755 "$builddir/$pkgname-cli" "$pkgdir/usr/bin/$pkgname-cli"
	install -Dm644 "$builddir/conf/sample.ini" "$pkgdir/etc/grafana.ini"
	install -dm755 "$pkgdir/usr/share/grafana"
	cp -r "$builddir/conf" "$builddir/public" "$pkgdir/usr/share/$pkgname/"
}

sha512sums="e2e0b9d1961dcbb3427a46cc4e8d711890aea9d2240c29ecf4741dd54864f7c766d1a3815a2fede552c8d01051651cbe67f83202efe2f8fe8be0b9405bdcdc61  grafana-7.3.3.tar.gz
0b982ac893f31c898865349f41efcb60ccf5789108d573677d9f33a5baf3cb532a8e80704bbd00154062de4d201a3e203f58569f1fd291fec2edfc0a2f434dfb  grafana-7.3.3-bin.tar.gz
b0a781e1b1e33741a97e231c761b1200239c6f1235ffbe82311fe883387eb23bef262ad68256ebd6cf87d74298041b53b947ea7a493cfa5aa814b2a1c5181e13  grafana.initd
c2d9896ae9a9425f759a47aeab42b7c43b63328e82670d50185de8c08cda7b8df264c8b105c5c3138b90dd46e86598b16826457eb3b2979a899b3a508cbe4e8c  grafana.confd"
